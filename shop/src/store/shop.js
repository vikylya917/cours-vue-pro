export default {
  state: {
    shopList: [
      {
        id: 1,
        title: 'Nike Red',
        descr:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper, libero et porta ullamcorper, ex lacus eleifend mauris, eget scelerisque massa ligula quis lorem. Praesent hendrerit eros id ex eleifend dictum sed ac nisl.',
        img: require('../assets/img/1.png'),
        gallery: [
          { name: 'Nike boots First', img: require('../assets/img/1.png') },
          { name: 'Nike boots Second', img: require('../assets/img/2.png') },
          { name: 'Nike boots Thrid', img: require('../assets/img/3.png') },
        ],
      },
      {
        id: 2,
        title: 'Nike Default',
        descr:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper, libero et porta ullamcorper, ex lacus eleifend mauris, eget scelerisque massa ligula quis lorem. Praesent hendrerit eros id ex eleifend dictum sed ac nisl. Etiam feugiat, quam quis tempus euismod, mi felis iaculis ipsum, vel porta dui turpis nec ipsum. Donec vitae consequat elit. Nam vel tincidunt orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis urna arcu, condimentum vel turpis sed, malesuada pretium augue. Nunc fermentum hendrerit nisi. Maecenas cursus justo ut ante sollicitudin, eget pellentesque neque egestas. Curabitur sollicitudin erat lorem, eu convallis nisl vehicula id.',
        img: require('../assets/img/4.png'),
        gallery: [
          { name: 'Nike boots First', img: require('../assets/img/4.png') },
          { name: 'Nike boots Second', img: require('../assets/img/5.png') },
          { name: 'Nike boots Thrid', img: require('../assets/img/6.png') },
        ],
      },
      {
        id: 3,
        title: 'Nike Grean',
        descr:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper, libero et porta ullamcorper, ex lacus eleifend mauris, eget scelerisque massa ligula quis lorem. Praesent hendrerit eros id ex eleifend dictum sed ac nisl. Etiam feugiat, quam quis tempus euismod, mi felis iaculis ipsum, vel porta dui turpis nec ipsum. Donec vitae consequat elit. Nam vel tincidunt orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis urna arcu, condimentum vel turpis sed, malesuada pretium augue. Nunc fermentum hendrerit nisi. Maecenas cursus justo ut ante sollicitudin, eget pellentesque neque egestas. Curabitur sollicitudin erat lorem, eu convallis nisl vehicula id.',
        img: require('../assets/img/7.png'),
        gallery: [
          { name: 'Nike boots First', img: require('../assets/img/7.png') },
          { name: 'Nike boots Second', img: require('../assets/img/8.png') },
          { name: 'Nike boots Thrid', img: require('../assets/img/9.png') },
        ],
      },
      {
        id: 4,
        title: 'Nike Street',
        descr:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur semper, libero et porta ullamcorper, ex lacus eleifend mauris, eget scelerisque massa ligula quis lorem. Praesent hendrerit eros id ex eleifend dictum sed ac nisl. Etiam feugiat, quam quis tempus euismod, mi felis iaculis ipsum, vel porta dui turpis nec ipsum. Donec vitae consequat elit. Nam vel tincidunt orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis urna arcu, condimentum vel turpis sed, malesuada pretium augue. Nunc fermentum hendrerit nisi. Maecenas cursus justo ut ante sollicitudin, eget pellentesque neque egestas. Curabitur sollicitudin erat lorem, eu convallis nisl vehicula id.',
        img: require('../assets/img/10.png'),
        gallery: [
          { name: 'Nike boots First', img: require('../assets/img/10.png') },
          { name: 'Nike boots Second', img: require('../assets/img/11.png') },
          { name: 'Nike boots Thrid', img: require('../assets/img/12.png') },
        ],
      },
    ],
  },
  mutations: {},
  actions: {},
  getters: {
    getShopList(state) {
      return state.shopList;
    },
    getProduct: state => id => {
      return state.shopList.find(product => product.id == id);
    },
  },
};
